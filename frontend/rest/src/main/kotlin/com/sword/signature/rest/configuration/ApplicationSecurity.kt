package com.sword.signature.rest.configuration

import com.sword.signature.business.configuration.OptionalFeaturesConfig
import com.sword.signature.rest.authentication.SecurityContextRepository
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders.WWW_AUTHENTICATE
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.adapter.ForwardedHeaderTransformer
import reactor.core.publisher.Mono
import java.net.URI

@Configuration
@EnableWebFluxSecurity
@OpenAPIDefinition(
    servers = [Server(url = "/api", description = "REST API URL")]
)
@ComponentScan(basePackages = ["com.sword.signature", "eu.coexya"])
class ApplicationSecurity (private val optionalFeaturesConfig: OptionalFeaturesConfig){

    @Bean
    fun securityWebFilterChain(
        http: ServerHttpSecurity,
        securityContextRepository: SecurityContextRepository
    ): SecurityWebFilterChain {
        http.csrf().disable()
        http.httpBasic().disable()
        http.securityContextRepository(securityContextRepository)
        http.authorizeExchange { exchanges ->
            // Authorize every OPTIONS request for browser verification purpose.
            exchanges.pathMatchers(HttpMethod.OPTIONS, "/**").permitAll()
            // Authorize the login endpoint to be accessed without authentication.
            exchanges.pathMatchers(HttpMethod.POST, "/auth").permitAll()
            // Authorize the forgotten password endpoint to be accessed without authentication.
            exchanges.pathMatchers(HttpMethod.POST, "/forgotten-password").permitAll()
            // Authorize the optional features endpoint to be accessed without authentication. Used to configure UI at start
            exchanges.pathMatchers(HttpMethod.GET, "/optionalFeatures").permitAll()
            if (optionalFeaturesConfig.registering.enabled) { // Authorize the register endpoint to be accessed without authentication.
                exchanges.pathMatchers(HttpMethod.POST, "/register").permitAll()
            }
            // Authorize the check endpoint for any user.
            exchanges.pathMatchers(HttpMethod.POST, "/check/**").permitAll()
            // Authorize all requests for swagger UI
            exchanges.pathMatchers(HttpMethod.GET, "/v3/api-docs").permitAll()
            exchanges.pathMatchers(HttpMethod.GET, "/v3/api-docs/**").permitAll()
            exchanges.pathMatchers(HttpMethod.GET, "/webjars/swagger-ui/**").permitAll()
            exchanges.pathMatchers(HttpMethod.GET, "/swagger-ui.html").permitAll()
            // Authorize actuator requests.
            exchanges.pathMatchers(HttpMethod.GET, "/actuator/**").permitAll()
            // Require an authentication for all API request apart from the login.
            exchanges.anyExchange().authenticated()
        }
        http.exceptionHandling().authenticationEntryPoint { exchange: ServerWebExchange, _: AuthenticationException ->
            val response = exchange.response
            response.statusCode = HttpStatus.UNAUTHORIZED
            val requestedWith = exchange.request.headers["X-Requested-With"]
            if (requestedWith == null || !requestedWith.contains("XMLHttpRequest")) {
                response.headers.set(
                    WWW_AUTHENTICATE,
                    String.format(WWW_AUTHENTICATE_FORMAT, DEFAULT_REALM)
                )
            }
            exchange.mutate().response(response)
            Mono.empty()
        }

        return http.build()
    }

    @Bean
    fun customOpenAPI(): OpenAPI {
        return OpenAPI()
            .components(
                Components()
                    .addSecuritySchemes(
                        "bearer-key",
                        SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("bearer").bearerFormat("JWT")
                    )
            )
    }

    @Bean
    fun forwardedHeaderTransformer(): ForwardedHeaderTransformer {
        return ForwardedHeaderTransformer()
    }

    @Bean
    fun indexRouter(): RouterFunction<ServerResponse> {
        val redirectToIndex = ServerResponse
            .temporaryRedirect(URI("/index.html"))
            .build()

        return router {
            GET("/") {
                redirectToIndex
            }
        }
    }

    companion object {
        private const val DEFAULT_REALM = "Realm"
        private const val WWW_AUTHENTICATE_FORMAT = "Basic realm=\"%s\""
    }
}
