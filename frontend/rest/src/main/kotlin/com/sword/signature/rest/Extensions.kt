package com.sword.signature.rest

import com.sword.signature.api.custom.OptionalFeatures
import com.sword.signature.business.configuration.OptionalFeaturesConfig

fun OptionalFeaturesConfig.toWeb(): OptionalFeatures {
    return OptionalFeatures(
        registering = OptionalFeatures.Registering(enabled = registering.enabled),
        keyGeneration = OptionalFeatures.KeyGeneration(enabled = keyGeneration.enabled)
    )
}
