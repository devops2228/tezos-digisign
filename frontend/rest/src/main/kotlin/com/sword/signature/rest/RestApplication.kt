package com.sword.signature.rest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RestApplication

fun main(args: Array<String>) {
    // Set base path for all controllers and swagger UI documentation
    System.setProperty("spring.webflux.base-path", "/api")
    runApplication<RestApplication>(*args)
}
