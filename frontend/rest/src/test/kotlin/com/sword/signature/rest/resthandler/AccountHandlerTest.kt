package com.sword.signature.rest.resthandler

import com.sword.signature.api.account.AccountCreate
import com.sword.signature.business.exception.HoneyPotFieldNotEmptyException
import com.sword.signature.business.exception.PasswordTooWeakException
import com.sword.signature.business.model.Account
import com.sword.signature.business.service.AccountService
import com.sword.signature.business.service.MailService
import com.sword.signature.rest.authentication.checkPassword
import com.sword.signature.business.configuration.OptionalFeaturesConfig
import com.sword.signature.webcore.authentication.JwtTokenService
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.stream.Stream
import com.sword.signature.business.model.AccountCreate as BusinessAccountCreate

internal class AccountHandlerTest(
) {

    private val accountService: AccountService = mockk()
    private val bCryptPasswordEncoder: BCryptPasswordEncoder = mockk()
    private val mailService: MailService = mockk()
    private val jwtTokenService: JwtTokenService = mockk()
    private val optionalFeaturesConfig: OptionalFeaturesConfig = OptionalFeaturesConfig()

    private val accountHandler = AccountHandler(
        accountService,
        bCryptPasswordEncoder,
        mailService,
        jwtTokenService,
        optionalFeaturesConfig
    )

    @BeforeEach
    fun resetMocks() {
        clearMocks(accountService, bCryptPasswordEncoder, mailService, jwtTokenService)
        optionalFeaturesConfig.registering = OptionalFeaturesConfig.Registering()
    }

    @Nested
    inner class RegisterUsesConfig() {
        private val expectedIsAdmin = false

        private val registeringAccount = AccountCreate(
            login = "login",
            email = "email",
            fullName = "fullName",
            company = "company",
            country = "country",
            tezosAccount = null,
            isAdmin = true,
            signatureLimit = null
        )

        private val emptyAccount = Account(
            id = "", login = "", email = "", fullName = "", company = "", password = "", country = "",
            publicKey = "", privateKey = "", hash = "", isAdmin = true, signatureLimit = null, disabled = false,
            firstLogin = false
        )

        @BeforeEach
        fun setMock() {
            // Mocking things we don't test
            every { bCryptPasswordEncoder.encode(any()) } returns ""
            every { jwtTokenService.generateVolatileToken(any(), any(), any()) } returns ""
            every { mailService.sendEmail(any()) } returns Unit
        }

        @ParameterizedTest
        @MethodSource("registerTestProvider")
        fun `register creates an account with correct parameters`(
            defaultPublicKey: String?,
            defaultPrivateKey: String?,
            defaultSignatureLimit: Int,
            expectedPublicKey: String?,
            expectedPrivateKey: String?,
            expectedSignatureLimit: Int
        ) {
            optionalFeaturesConfig.registering.defaultSignatureLimit = defaultSignatureLimit
            optionalFeaturesConfig.registering.defaultPublicKey = defaultPublicKey
            optionalFeaturesConfig.registering.defaultPrivateKey = defaultPrivateKey

            val accountHandler = AccountHandler(
                accountService,
                bCryptPasswordEncoder,
                mailService,
                jwtTokenService,
                optionalFeaturesConfig
            )

            // Prepare the capture of the argument passed to the service by the handler
            val argument = slot<BusinessAccountCreate>()
            coEvery {
                accountService.createAccount(any(), capture(argument))
            } returns emptyAccount// Mocks createAccount, not tested


            runBlocking {
                accountHandler.register("", registeringAccount) // Tested endpoint

                val accountCaptured = argument.captured // Captured at the call of createAccount()

                assertEquals(registeringAccount.login, accountCaptured.login)
                assertEquals(registeringAccount.email, accountCaptured.email)
                assertEquals(registeringAccount.fullName, accountCaptured.fullName)
                assertEquals(registeringAccount.company, accountCaptured.company)
                assertEquals(registeringAccount.country, accountCaptured.country)

                assertEquals(expectedPublicKey, accountCaptured.tezosAccount?.publicKey)
                assertEquals(expectedPrivateKey, accountCaptured.tezosAccount?.privateKey)
                assertEquals(expectedIsAdmin, accountCaptured.isAdmin)
                assertEquals(expectedSignatureLimit, accountCaptured.signatureLimit)
            }
        }

        fun registerTestProvider(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    "publicKey",
                    "privateKey",
                    5,
                    "publicKey",
                    "privateKey",
                    5
                ),
                Arguments.of(
                    null,
                    "privateKey",
                    5,
                    null,
                    null,
                    5
                ),
                Arguments.of(
                    "publicKey",
                    null,
                    5,
                    null,
                    null,
                    5
                ),
                Arguments.of(
                    null,
                    null,
                    5,
                    null,
                    null,
                    5
                )
            )

        }

    }

    @Test
    fun `Can't register if honeypot fields are filled`() {
        val botRegistration1 = AccountCreate(
            login = "login",
            email = "email",
            fullName = "fullName",
            company = "company",
            country = "country",
            tezosAccount = null,
            isAdmin = true,
            signatureLimit = null,
            honeyPotField1 = "Some spam text"
        )

        val botRegistration2 = AccountCreate(
            login = "login",
            email = "email",
            fullName = "fullName",
            company = "company",
            country = "country",
            tezosAccount = null,
            isAdmin = true,
            signatureLimit = null,
            honeyPotField2 = "Some other spam text"
        )

        val botRegistration3 = AccountCreate(
            login = "login",
            email = "email",
            fullName = "fullName",
            company = "company",
            country = "country",
            tezosAccount = null,
            isAdmin = true,
            signatureLimit = null,
            honeyPotField1 = "did you know spam is also brand of canned pork ?",
            honeyPotField2 = "spamming"
        )

        assertThrows<HoneyPotFieldNotEmptyException> {
            runBlocking { accountHandler.register("", botRegistration1) }
        }
        assertThrows<HoneyPotFieldNotEmptyException> {
            runBlocking { accountHandler.register("", botRegistration2) }
        }
        assertThrows<HoneyPotFieldNotEmptyException> {
            runBlocking { accountHandler.register("", botRegistration3) }
        }
    }

    @Test
    fun `password too short`() {
        assertThrows<PasswordTooWeakException> {
            checkPassword("S+tr0ng")
        }
    }

    @Test
    fun `password too weak`() {
        assertThrows<PasswordTooWeakException> {
            checkPassword("tooweak01")
        }
    }

    @Test
    fun `password strong`() {
        checkPassword("S+tr0ng!")
    }
}
