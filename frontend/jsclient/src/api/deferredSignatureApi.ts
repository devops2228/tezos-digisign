import {DeferredFile, DeferredFileCriteria, DeferredSignatureRequest, DeferredSignatureResponse} from "@/api/types"
import {AxiosRequestConfig, AxiosResponse} from "axios"

import {Api} from "@/api/api"
import {apiConfig} from "@/api/api.config"

export const API_DEFERRED_SIGN = "/deferred/sign"
export const API_DEFERRED_GET = "/deferred/files"
export const API_DEFERRED_COUNT = "/deferred/files-count"

export class DeferredSignatureApi extends Api {
    public constructor(config: AxiosRequestConfig) {
        super(config)
    }

    public list(criteria: DeferredFileCriteria = {}): Promise<Array<DeferredFile>> {
        return this.get<Array<DeferredFile>>(API_DEFERRED_GET, criteria)
            .then((response: AxiosResponse<Array<DeferredFile>>) => {
                return response.data
            })
    }

    public count(criteria: DeferredFileCriteria = {}): Promise<number> {
        return this.get<number>(API_DEFERRED_COUNT, criteria)
            .then((response: AxiosResponse<number>) => {
                return response.data
            })
    }

    public uploadFilesDeferredSignature(request: DeferredSignatureRequest): Promise<DeferredSignatureResponse> {
        return this.post<DeferredSignatureResponse, DeferredSignatureRequest>(API_DEFERRED_SIGN, {}, request)
            .then((response: AxiosResponse<DeferredSignatureResponse>) => {
                return response.data
            })
    }
}

export const deferredSignatureApi = new DeferredSignatureApi(apiConfig)
