import {signatureApi} from "@/api/signatureApi"
import {
    CheckStatus,
    DeferredSignatureRequest,
    DeferredSignatureResponse,
    FileCheck,
    RetrySignatureRequest,
    RetrySignatureResponse,
    SignatureCheckRequest,
    SignatureCheckResponse,
    SignatureMultiRequest,
    SignatureResponse,
} from "@/api/types"
import {Action, Module, Mutation, VuexModule} from "vuex-class-modules"
import * as CryptoJS from "crypto-js"

interface ProofUpdate {
    index: number,
    proof: File
}

interface CheckResponseUpdate {
    index: number,
    checkResponse: SignatureCheckResponse | undefined
}

@Module
export default class SignaturesModule extends VuexModule {

    private checkResponse: SignatureCheckResponse | undefined = undefined

    private fileChecks: Array<FileCheck> = []
    private checkStatus: CheckStatus = {
        loading: false,
        nbElementChecked: 0,
    }

    private deferredSignatureResponse: DeferredSignatureResponse | undefined = undefined
    private signResponse: Array<SignatureResponse> = []
    private retrySignatureResponse: RetrySignatureResponse | undefined = undefined

    @Action
    public check(sigCheck: SignatureCheckRequest) {
        signatureApi.check(sigCheck).then((response: SignatureCheckResponse) => {
            this.setCheckResponse(response)
        }).catch((_: Error) => {
                this.setCheckResponse({
                    output: "KO",
                    error: "INTERNAL_SERVOR_ERROR",
                })
            },
        )
    }

    @Action
    public async checkFiles() {
        this.startCheckStatus()

        // Reset the responses for visual effect
        this.fileChecks.forEach((value, index) => this.setFileCheckResponse({index, checkResponse: undefined}))

        for (const fileCheck of this.fileChecks) {
            const index = this.fileChecks.indexOf(fileCheck)

            const hash = await this.computeHash(fileCheck.file)
            const request: SignatureCheckRequest = {
                documentHash: hash,
                proof: fileCheck.proofFile,
            }

            signatureApi.check(request).then((checkResponse: SignatureCheckResponse) => {
                this.setFileCheckResponse({index, checkResponse})
                this.incrCheckStatus()
            }).catch((_: Error) => {
                const checkResponse = {
                    output: "KO",
                    error: "INTERNAL_SERVOR_ERROR",
                }
                this.setFileCheckResponse({index, checkResponse})
            })
        }
        this.stopCheckStatus()
    }

    @Mutation
    public resetFileChecks() {
        this.fileChecks = []
    }

    @Mutation
    public addFileCheck(fileCheck: FileCheck) {
        this.fileChecks.push(fileCheck)
    }

    @Mutation
    public removeFileCheck(index: number) {
        this.fileChecks.splice(index, 1)
    }

    @Mutation
    public setFileCheckProof(proofUpdate: ProofUpdate) {
        this.fileChecks[proofUpdate.index].proofFile = proofUpdate.proof
    }

    @Mutation
    public setFileCheckResponse(checkResponse: CheckResponseUpdate) {
        this.fileChecks[checkResponse.index].checkResponse = checkResponse.checkResponse
    }

    public getFileChecks(): Array<FileCheck> {
        return this.fileChecks
    }

    public async signMulti(sign: SignatureMultiRequest) {
        await signatureApi.signMulti(sign).then((response: Array<SignatureResponse>) => {
            this.setSignResponse(response)
        })
    }

    public async uploadFilesDeferredSignature(request: DeferredSignatureRequest) {
        await signatureApi.uploadFilesDeferredSignature(request).then((response: DeferredSignatureResponse) => {
            this.setDeferredSignatureResponse(response)
        })
    }

    @Mutation
    public setDeferredSignatureResponse(response: DeferredSignatureResponse) {
        this.deferredSignatureResponse = response
    }

    public async retrySignature(request: RetrySignatureRequest) {
        await signatureApi.retrySignature(request).then((response: RetrySignatureResponse) => {
            this.setRetrySignatureResponse(response)
        })
    }

    @Mutation
    public setSignResponse(sign: Array<SignatureResponse>) {
        this.signResponse = sign
    }

    @Mutation
    public setRetrySignatureResponse(response: RetrySignatureResponse) {
        this.retrySignatureResponse = response
    }

    public get getRetrySignatureResponse(): RetrySignatureResponse | undefined {
        return this.retrySignatureResponse
    }

    @Action
    public reset() {
        this.setCheckResponse(undefined)
        this.resetFileChecks()
        this.resetCheckStatus()
    }

    @Mutation
    public setCheckResponse(check: SignatureCheckResponse | undefined) {
        this.checkResponse = check
    }

    public getCheckResponse(): SignatureCheckResponse | undefined {
        return this.checkResponse
    }

    public getCheckStatus(): CheckStatus {
        return this.checkStatus
    }

    @Mutation
    private startCheckStatus() {
        this.checkStatus = {
            loading: true,
            nbElementChecked: 0,
        }
    }

    @Mutation
    private stopCheckStatus() {
        this.checkStatus.loading = false
    }

    @Mutation
    private resetCheckStatus() {
        this.checkStatus = {
            loading: false,
            nbElementChecked: 0,
        }
    }

    @Mutation
    private incrCheckStatus() {
        this.checkStatus.nbElementChecked += 1
    }

    private computeHash(file: File): Promise<string> {
        return new Promise((async (resolve) => {
            const reader = new FileReader()
            reader.onloadend = (e) => {
                const wordArray = CryptoJS.lib.WordArray.create(e.target!.result)
                resolve(CryptoJS.SHA256(wordArray).toString())
            }
            reader.readAsArrayBuffer(file)
        }))
    }

}
