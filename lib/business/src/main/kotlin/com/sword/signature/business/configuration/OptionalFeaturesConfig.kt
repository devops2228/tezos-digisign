package com.sword.signature.business.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "optional-features")
class OptionalFeaturesConfig {

    var registering: Registering = Registering()
    var keyGeneration: KeyGeneration = KeyGeneration()

    class Registering {
        var enabled: Boolean = false
        var defaultSignatureLimit: Int = 10
        var defaultPublicKey: String? = null
        var defaultPrivateKey: String? = null
    }

    class KeyGeneration {
        var enabled: Boolean = false
        var defaultProviderPublicKey: String? = null
        var defaultProviderSecretKey: String? = null
        var defaultAmount: Long = 10000000
        val revealCost: Long = 5297
    }
}
