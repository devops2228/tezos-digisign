package com.sword.signature.business.model

import com.sword.signature.business.exception.InvalidSignatureLimitValueException

data class AccountCreate(
    val login: String,
    val email: String,
    val password: String,
    val fullName: String?,
    val company: String?,
    val country: String?,
    val tezosAccount: TezosAccount?,
    val isAdmin: Boolean = false,
    val disabled: Boolean = false,
    val signatureLimit: Int? = null,
    val isTezosAccountInConfig: Boolean? = null
) {
    init {
        if (signatureLimit != null && signatureLimit < 0) {
            throw InvalidSignatureLimitValueException()
        }
    }
}

