package com.sword.signature.business.service

import com.sword.signature.business.model.*
import com.sword.signature.common.criteria.DeferredSignatureFileCriteria
import kotlinx.coroutines.flow.Flow
import org.springframework.data.domain.Pageable


interface DeferredSignatureService {

    /**
     * Retrieve all files matching the filter.
     * @param requester The account requesting the files.
     * @param criteria Criteria for file search.
     * @param pageable Pageable for memory-optimized file retrieval.
     * @return Files matching the filter.
     */
    suspend fun getFiles(
        requester: Account,
        criteria: DeferredSignatureFileCriteria? = null,
        pageable: Pageable = Pageable.unpaged()
    ): Flow<DeferredSignatureFile>

    /**
     * Count the number of files matching the filter.
     * @param requester The account requesting the number of files.
     * @param criteria Filter for file search.
     * @return Number of files matching the filter.
     */
    suspend fun countFiles(requester: Account, criteria: DeferredSignatureFileCriteria?): Long

    /**
     * Delete the file with the provided id.
     * @param requester Account requesting the file removal.
     * @param id Id of the file to remove.
     */
    suspend fun deleteSignatureFile(requester: Account, id: String)

    /**
     * Upload a file for a deferred signature
     * @param requester Account requesting the signature
     * @param signatureFileDetails Details about the file to sign
     */
    suspend fun deferredSignature(
        requester: Account,
        signatureFileDetails: DeferredSignatureFileCreate,
    ): DeferredSignatureFile

}
