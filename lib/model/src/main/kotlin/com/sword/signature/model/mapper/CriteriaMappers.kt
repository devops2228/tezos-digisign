package com.sword.signature.model.mapper

import com.querydsl.core.BooleanBuilder
import com.querydsl.core.types.Predicate
import com.querydsl.core.types.dsl.Expressions
import com.sword.signature.common.criteria.DeferredSignatureFileCriteria
import com.sword.signature.common.criteria.FileCriteria
import com.sword.signature.common.criteria.JobCriteria
import com.sword.signature.common.criteria.TreeElementCriteria
import com.sword.signature.common.enums.TreeElementType
import com.sword.signature.model.entity.*
import java.time.ZoneOffset

fun FileCriteria.toPredicate(): Predicate {
    val predicates = ArrayList<Predicate>()

    val qTreeElementEntity = QTreeElementEntity("treeElementEntity")

    predicates.add(qTreeElementEntity.type.eq(TreeElementType.LEAF))

    if (id?.isNotBlank() == true) {
        predicates.add(qTreeElementEntity.id.eq(id))
    }

    if (name?.isNotBlank() == true) {
        predicates.add(qTreeElementEntity.metadata.fileName.containsIgnoreCase(name))
    }

    if (hash?.isNotBlank() == true) {
        predicates.add(qTreeElementEntity.hash.equalsIgnoreCase(hash))
    }

    if (jobId?.isNotBlank() == true) {
        predicates.add(qTreeElementEntity.jobId.eq(jobId))
    }

    if (jobIds != null) {
        predicates.add(qTreeElementEntity.jobId.`in`(jobIds))
    }

    return andTogetherPredicates(predicates)
}

fun DeferredSignatureFileCriteria.toPredicate(): Predicate {
    val predicates = ArrayList<Predicate>()

    val qDeferredFileEntity = QDeferredSignatureFileEntity("deferredSignatureFileEntity")

    if (id?.isNotBlank() == true) {
        predicates.add(qDeferredFileEntity.id.eq(id))
    }

    if (hash?.isNotBlank() == true) {
        predicates.add(qDeferredFileEntity.hash.equalsIgnoreCase(hash))
    }

    if (accountId?.isNotBlank() == true) {
        predicates.add(qDeferredFileEntity.userId.eq(accountId))
    }

    if (name?.isNotBlank() == true) {
        predicates.add(qDeferredFileEntity.metadata.fileName.containsIgnoreCase(name))
    }

    return andTogetherPredicates(predicates)
}

inline fun <reified T : TezosOperationJobEntity> JobCriteria.toPredicate(): Predicate {
    val predicates = ArrayList<Predicate>()
    val qTezosOperationJobEntity = QTezosOperationJobEntity("blop")

    if (id?.isNotBlank() == true) {
        predicates.add(
            qTezosOperationJobEntity.id.eq(id)
        )
    }

    if (accountId?.isNotBlank() == true) {
        predicates.add(
            qTezosOperationJobEntity.userId.eq(accountId)
        )
    }

    if (dateStart != null) {
        predicates.add(
            qTezosOperationJobEntity.createdDate.after(dateStart!!.atStartOfDay().atOffset(ZoneOffset.UTC))
        )
    }

    if (dateEnd != null) {
        predicates.add(
            qTezosOperationJobEntity.createdDate.before(dateEnd!!.plusDays(1).atStartOfDay().atOffset(ZoneOffset.UTC))
        )
    }

    if (jobState != null) {
        predicates.add(
            qTezosOperationJobEntity.state.eq(jobState)
        )
    }

    if (T::class == JobEntity::class) {
        val qJobEntity = QJobEntity("blop")

        if (flowName?.isNotBlank() == true) {
            predicates.add(
                qJobEntity.flowName.containsIgnoreCase(flowName)
            )
        }

        if (channelName?.isNotBlank() == true) {
            predicates.add(
                qJobEntity.channelName.containsIgnoreCase(channelName)
            )
        }
    }

    // Excludes when T is parent TezosOperationJobEntity
    when (T::class) {
        JobEntity::class,
        TransferJobEntity::class,
        RevealJobEntity::class ->
            predicates.add(
                BooleanBuilder().apply {
                    or(
                        Expressions.stringPath(qTezosOperationJobEntity, "_class").eq(T::class.qualifiedName)
                    )
                    or(
                        Expressions.stringPath(qTezosOperationJobEntity, "_class").eq(T::class.simpleName)
                    )
                }
            )
    }

    return andTogetherPredicates(predicates)
}

fun TreeElementCriteria.toPredicate(): Predicate {

    val predicates = ArrayList<Predicate>()

    val qTreeElementEntity = QTreeElementEntity("blop")

    if (this.notId?.isNotBlank() == true) {
        predicates.add(
            qTreeElementEntity.id.ne(this.notId)
        )
    }

    if (this.parentId?.isNotBlank() == true) {
        predicates.add(
            qTreeElementEntity.parentId.eq(this.parentId)
        )
    }

    if (this.jobId?.isNotBlank() == true) {
        predicates.add(
            qTreeElementEntity.jobId.eq(this.jobId)
        )
    }

    if (this.type != null) {
        predicates.add(
            qTreeElementEntity.type.eq(this.type)
        )
    }
    return andTogetherPredicates(predicates)
}

fun andTogetherPredicates(predicates: List<Predicate>) = BooleanBuilder().apply {
    predicates.forEach {
        and(it)
    }
}
