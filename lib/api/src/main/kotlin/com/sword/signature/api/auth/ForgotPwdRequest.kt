package com.sword.signature.api.auth

data class ForgotPwdRequest(
    val usernameOrEmail: String
)
