[Index](../README.md) 

# Docker Deployment

## Deployment options

### Sandbox node

Components:
* MongoDB database
* MongoDB administration UI
* FakeSMTP server
* Tezos sandbox node
* Tzindex block indexer
* Tezos Signature REST server (+ client)
* Tezos Signature daemon

Configuration:
* Daemon configuration file: **compose-config/sandbox/application-daemon.yml**.
* REST server configuration file: **compose-config/sandbox/application-rest.yml**.
* To add the secure option to the cookie: edit the variable *UNSECURED_TOKEN* in the **docker-compose.sandbox.yml** file. *0* to enable it and *1* to disable it.

### Granadanet mode

Components:
* MongoDB database
* MongoDB backup
* MongoDB administration UI
* FakeSMTP server
* Tezos Signature REST server (+ client)
* Tezos Signature daemon

Configuration:
* Daemon configuration file: **compose-config/testnet/application-daemon.yml**.
* REST server configuration file: **compose-config/testnet/application-rest.yml**.
* To add your tezos keys: edit the daemon configuration (property *tezos.keys.admin*).
* To add the secure option to the cookie: edit the variable *UNSECURED_TOKEN* in the **docker-compose.testnet.yml**
  file. *0* to enable it and *1* to disable it.

### Mainnet mode

Components:
* MongoDB database
* MongoDB backup
* MongoDB administration UI
* FakeSMTP server
* Tezos Signature REST server (+ client)
* Tezos Signature daemon

Configuration:
* Daemon configuration file: **compose-config/mainnet/application-daemon.yml**.
* REST server configuration file: **compose-config/mainnet/application-rest.yml**.
* To add your tezos keys: edit the daemon configuration (property *tezos.keys.admin*).
* To add the secure option to the cookie: edit the variable *UNSECURED_TOKEN* in the **docker-compose.mainnet.yml** file. *0* to enable it and *1* to disable it.

The only difference between **Testnet mode** and **Mainnet mode** is the configuration (node URL, smart contract address, tezos keys).

## Deploy the solution

###Create SSL certificates

* Create new certificates
```
./scripts/create_ssl_certificate.sh [your_server_name]
```
The parameter server_name will be the URL address you will access your service to.

If you are using the web browser Mozilla Firefox, you will have to add your certificate to the list of trusted certificates.
To do so :
* Open your browser
* Click on the **Open menu** button 
* Choose the **options** (or **settings** depending on the version)
* Open the **Privacy & Security** tab
* Scroll down until you reach the **Certificates** section and click on **View Certificates**
* Under **Authorities** tab, click on **import**
* Select your certificate *your_server_name.crt* in *reverseProxy/cert* folder
* In **Downloading Certificate** window, set up the checkboxes **Trust this CA to identify websites** and **Trust this CA to identify email users** and click **OK**
* Close your browser to restart it

### Docker images build (*either build or import*)

* Clean the project (in case of refresh of the source code)
```
./gradlew clean
```

* Build all services
```
./gradlew assemble
```

* Build the images (after each code update)

#### Sandbox

```
docker-compose -f docker-compose.sandbox.yml build
```

#### Granadanet

```
docker-compose -f docker-compose.testnet.yml build
```

#### Mainnet

```
docker-compose -f docker-compose.mainnet.yml build
```

### Docker images import (*either build or import*)

* Import the docker images of each provided service
```
docker load -i TAR_FILE
```

### Run the services

#### Sandbox

* Run the services
```
docker-compose -f docker-compose.sandbox.yml up -d
```
* The same way as development mode, for sandbox you need to originate the contract.
```
(cd contract; make originate)
```
* Then, edit the file **compose-config/sandbox/application-daemon.yml** and replace the property *tezos.contract.address* by the contract address.
* Do the same update to the file **compose-config/sandbox/application-rest.yml**.
* Finally, restart the daemon and rest containers:
```
docker restart tezos-digisign-daemon tezos-digisign-rest
```

#### Granadanet

* Run the services
```
docker-compose -f docker-compose.testnet.yml up -d
```

#### Mainnet:

* Run the services
```
docker-compose -f docker-compose.mainnet.yml up -d
```

### Stop the services

#### Sandbox

* Stop the services (add **-v** to reset database)
```
docker-compose -f docker-compose.sandbox.yml down
```

#### Granadanet

* Stop the services (add **-v** to reset database)
```
docker-compose -f docker-compose.testnet.yml down
```

#### Mainnet

* Stop the services (add **-v** to reset database)
```
docker-compose -f docker-compose.mainnet.yml down
```

### Access the services

* Access the UI at `https://your_server_name:443` (default credentials: admin/Sword@35)
* Access the REST API documentation at `https://your_server_name:443/api/swagger-ui.html`
* Access the MongoDB UI at `http://localhost:8081`
* To authenticate on the REST API: click on the **Authorize** button and provide the access token (retrieved through the UI)

### Recover the database from a backup

By default, the backup container will create daily backup and stored them in a docker volume or filesystem directory.
The backup files naming convention is **backup-yyyyMMdd_HHmmss.tar.gz**.

In order to import a backup, you need to mount the backup as a docker volume for the database container and then execute the mongorestore command within the database container context.

**Warning** : be careful not to have launched Digisign daemon before importing the backup. Otherwise the database will already be initialized and you will have issues with duplicated migrations. The database needs to be empty for the backup to be restored without any error occuring afterwards.

#### Recovery example with docker compose

* Add the backup as volume.
```yaml
services:
  mongo:
    ...
    volumes:
      - 'mongo-data:/data/db'
      - './backup-yyyyMMdd_HHmmss.tar.gz:/backup.tar.gz'
```

* Launch only the database with docker-compose
```shell
docker-compose -f docker-compose.ENV.yml up -d mongo
```

* Access the database container shell
```shell
docker exec -it tezos-digisign-mongo bash
```

* Restore the backup within the database container context
```shell
mongorestore --uri mongodb://root:password@localhost:27017 --gzip --archive=/backup.tar.gz
```

* Exit the container (Ctrl+D or *exit*)

* Launch all the services with docker-compose
```shell
docker-compose -f docker-compose.ENV.yml up -d
```
