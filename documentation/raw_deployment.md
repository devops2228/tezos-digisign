[Index](../README.md)

# Tezos Digisign - Raw deployment

This raw deployment is for people who does not want to use docker. Docker is used only for the database and the fake SMTP server.
To build the VM with the requirement, it's possible to use vagrant+VirtualBox. A vagrant file with the right external port and ansible script is available: [deployment](../deployment/vagrant).  
This deployment is based on the Florencenet Tezos Network. The Tezos account signing the transaction is already configured (please, use it sparingly).  

## Requirements

* Docker (for development) and Docker compose.
* Jdk 11
* node.js  

## VM deployment with vagrant

VirtualBox and Vagrant must have been installed on your computer.
After downloading [deployment](../deployment/vagrant), go into the directory vagrant and launch the command:  
`vagrant up`
  
connect to the VM:  
`vagrant ssh`  

## install the jdk and node.js

* Jdk 11  
`sudo apt update`  
`sudo apt install default-jdk`  
* node.js  
`curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh`  
`sudo bash nodesource_setup.sh`     
`sudo apt install nodejs`  

## Deployment

Retrieve the code digisign  
`git clone https://gitlab.com/coexya/tezos/tezos-digisign.git`  
`cd tezos-digisign`  

Compile    
`./gradlew unpack`

Launch Tezos Digisign:    
`docker-compose -f docker-compose.plain.yml up -d`

Launch the daemon:  
`java -Djdk.tls.client.protocols=TLSv1.2 -cp compose-config/testnet_plain/daemon/.:backend/daemon/build/unpacked/BOOT-INF/classes:backend/daemon/build/unpacked/BOOT-INF/lib/* com.sword.signature.daemon.DaemonApplicationKt &`

Launch the rest service:  
`java -Djdk.tls.client.protocols=TLSv1.2 -cp compose-config/testnet_plain/rest/.:frontend/rest/build/unpacked/BOOT-INF/classes:frontend/rest/build/unpacked/BOOT-INF/lib/* com.sword.signature.rest.RestApplicationKt &`

In a Web browser, you can connect to Digisign:

Connect to the website (the login/password is admin/Sword@35):  
`http://localhost:9090/index.html#/signature-check`
  
Connect to the Tezos Index and check the storage (after signing a document):  
`https://ghost.tzstats.com/KT1PTTUwkPMDrwAtNfD3JZP93wH1DkycXPSH`

Connect to the Tezos Index and check the signer account:  
`https://ghost.tzstats.com/tz1UXWbxbm9NXkmmN4UqQxet5ttH6PsfEn3V`

  
Connect to the database with mongo express:  
`http://localhost:8081/#/login`
  
Check the api with swagger:  
`http://localhost:9090/api/swagger-ui.html`
  
Check the fake SMTP server:  
`http://localhost:5080/`


## Stop the VM with vagrant

Quit the linux machine:  
`exit`  

Stop the VM:  
`vagrant halt`  

