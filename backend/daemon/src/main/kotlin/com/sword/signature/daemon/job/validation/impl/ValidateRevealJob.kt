package com.sword.signature.daemon.job.validation.impl

import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.model.Account
import com.sword.signature.business.model.integration.JobPayloadType
import com.sword.signature.business.model.integration.TezosOperationAnchorJobPayload
import com.sword.signature.business.model.integration.TezosOperationValidationJobPayload
import com.sword.signature.business.service.AccountService
import com.sword.signature.business.service.JobService
import com.sword.signature.common.enums.TezosAccountStateType
import com.sword.signature.daemon.job.Metrics
import com.sword.signature.daemon.job.validation.TezosOperationValidationJob
import com.sword.signature.daemon.logger
import com.sword.signature.daemon.sendPayload
import com.sword.signature.model.repository.AccountRepository
import eu.coexya.tezos.connector.service.TezosReaderService
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import org.springframework.beans.factory.annotation.Value
import org.springframework.messaging.MessageChannel
import org.springframework.stereotype.Component
import java.time.Duration

@Component
class ValidateRevealJob(
    override val jobService: JobService,
    override val accountService: AccountService,
    override val tezosReaderService: TezosReaderService,
    override val anchoringMessageChannel: MessageChannel,
    override val validationRetryMessageChannel: MessageChannel,
    @Value("\${tezos.validation.minDepth}") override val minDepth: Long,
    @Value("\${daemon.validation.timeout}") override val validationTimeout: Duration,
    override val metrics: Metrics,

    private val accountRepository: AccountRepository
) : TezosOperationValidationJob {

    override suspend fun onValidated(
        requester: Account,
        payload: TezosOperationValidationJobPayload
    ) {
        val revealJob = jobService.findByIdRevealJob(requester, payload.jobId)
            ?: throw IllegalStateException("RevealJob with id = ${payload.jobId} should exist")

        val accountToUpdate = accountRepository.findById(revealJob.accountToUpdate).awaitFirstOrNull()
            ?: throw EntityNotFoundException("account", revealJob.accountToUpdate)

        val toPatch = accountToUpdate.copy(
            tezosAccountState = TezosAccountStateType.DEFINED
        )
        accountRepository.save(toPatch).awaitSingle()
    }

    override suspend fun onValidationFailed(requester: Account, payload: TezosOperationValidationJobPayload) {
        anchoringMessageChannel.sendPayload(
            TezosOperationAnchorJobPayload(
                type = JobPayloadType.REVEAL,
                requesterId = requester.id,
                jobId = payload.jobId
            )
        )
    }

    companion object {
        val LOGGER = logger()
    }

}
