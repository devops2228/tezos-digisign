package com.sword.signature.daemon.configuration

import kotlinx.coroutines.sync.Mutex
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap

@Component
class KeyLockStore {

    private val concurrentLocks = ConcurrentHashMap<String, Mutex>()

    @Synchronized
    fun getLock(key: String): Mutex {
        LOGGER.trace("Retrieving lock for key '{}'", key)
        return concurrentLocks.getOrPut(key) { Mutex(false) }
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(KeyLockStore::class.java)
    }
}
