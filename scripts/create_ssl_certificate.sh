#!/usr/bin/env bash

check_parameters(){
  if [ "$#" -ne 1 ]; then
    echo "Usage: $0 [server_name]"
    exit 1
  fi
}
modify_docker_compose(){
  sed -i "s|certs/[^:]*\.crt|certs/$1.crt|g; s|certs/[^:]*\.key|certs/$1.key|g" "$2"
}

modify_nginx_config(){
  sed -i "s|certs/[^;]*\.crt|certs/$1.crt|g; s|certs/[^;]*\.key|certs/$1.key|g" "$2"
  sed -i "s|server_name\s[^;]*|server_name $1|g" "$2"
}

main(){
  projectDir=$(dirname "$0")/..

  check_parameters "$@"
  openssl req -newkey rsa:2048 -nodes -keyout $projectDir/reverseProxy/certs/"$1".key -x509 -days 365 -addext "subjectAltName = DNS:$1" -subj "/C=FR/ST=BRETAGNE/L=RENNES/O=COEXYA/CN=$1" -out $projectDir/reverseProxy/certs/"$1".crt

  if ! grep -q "127.0.0.1 $1" /etc/hosts; then
    echo "127.0.0.1 $1" | sudo tee -a /etc/hosts
  fi

  modify_docker_compose "$1" $projectDir/docker-compose.sandbox.yml
  modify_docker_compose "$1" $projectDir/docker-compose.sandbox.d.yml
  modify_docker_compose "$1" $projectDir/docker-compose.mainnet.yml
  modify_docker_compose "$1" $projectDir/docker-compose.mainnet.d.yml
  modify_docker_compose "$1" $projectDir/docker-compose.testnet.yml
  modify_docker_compose "$1" $projectDir/docker-compose.testnet.d.yml

  modify_nginx_config "$1" $projectDir/reverseProxy/reverseproxy.conf

  exit 0
}

main "$@"
