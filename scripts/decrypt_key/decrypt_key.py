from pytezos import Key
import sys

def compute_unencrypted_secret_key(argv):
    encrypted_secret_key = argv[0]
    password = argv[1]
    key = Key.from_encoded_key(encrypted_secret_key, passphrase=password.encode())
    
    print("Tezos address: ", key.public_key_hash())
    print("Public key: ", key.public_key())
    print("Secret key: ", key.secret_key())


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: $0 encrypted_secret_key password")
        sys.exit(0)
    compute_unencrypted_secret_key(sys.argv[1:])
