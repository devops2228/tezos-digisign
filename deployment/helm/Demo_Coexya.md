# Digisign Coexya Demo deployment with Helm

## Persistent volume

Tezos keys needs to be added to the file **settings/digisign-demo/config/application-daemon.yml** from the wiki.

The directory **settings/tediji-demo/config** is required to be copied to the NFS mount of the Kubernetes cluster (server **edivdockerman1.ren-dev2.lan**, folder **/nfs/tediji-demo**). The folder **data/mongo** needs to be created within the mount folder.

The expected file structure of the NFS mounted folder is:
```
/config
  /application-daemon.yml
  /rp.conf
/data
  /mongo
```

## Helm chart deployment

The deployment is divided in 2 parts :
* Digisign deployment on Rennes K8S cluster (internal URL: http://digisign-demo.k8s.ren-dev2.lan) ;
* reverse proxy deployment on Lyon K8S cluster to expose the demo to the web (external URL: https://tezos-signature-rest.k8s.coexya.eu/).

In order to deploy on these 2 clusters, K8S authentication file are required: **edivici.yaml** for Rennes cluster et **lyok8sclu01.yaml** for Lyon cluster.

### Digisign deployment

* To install Digisign:
```shell
helm install digisign-demo ./digisign -n digisign-demo --kubeconfig ~/.kube/edici.yaml -f settings/digisign-demo/settings.yaml
```

* To update Digisign:
```shell
helm upgrade digisign-demo ./digisign -n digisign-demo --kubeconfig ~/.kube/edici.yaml -f settings/digisign-demo/settings.yaml
```

* To uninstall Digisign (without data loss):
```shell
helm uninstall digisign-demo -n digisign-demo --kubeconfig ~/.kube/edici.yaml
```

### Reverse proxy deployment

* To install the reverse proxy:
```shell
helm install digisign-demo-rp ./digisign-rp -n tezos --kubeconfig ~/.kube/lyok8sclu01.yaml -f settings/digisign-rp-demo/settings.yaml
```

* To update the reverse proxy:
```shell
helm upgrade digisign-demo-rp ./digisign-rp -n tezos --kubeconfig ~/.kube/lyok8sclu01.yaml -f settings/digisign-rp-demo/settings.yaml
```

* To uninstall the reverse proxy:
```shell
helm uninstall digisign-demo-rp -n tezos --kubeconfig ~/.kube/lyok8sclu01.yaml
```
