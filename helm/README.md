# Helm deployment

## Deploy Digisign with helm

* Replace variables HASH, PUBLICKEY, SECRETKEY, SECRET in **ENV.yaml**.
Please refer to [Keys Mangement](../documentation/keys-management.md) for key generation.

* Deploy the helm chart.
```shell
helm install -f ENV.yaml  digisign ./digisign --namespace digisign --create-namespace
```

## Undeploy Digisign
* Undeploy the helm chart.
```shell
helm uninstall -f ENV.yaml  digisign ./digisign --namespace digisign
```

## Update Digisign

* Update the helm chart.
```shell
helm upgrade -f ENV.yaml  digisign ./digisign --namespace digisign
```
